"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsx_runtime_1 = require("react/jsx-runtime");
var react_dom_1 = __importDefault(require("react-dom"));
react_dom_1.default.render((0, jsx_runtime_1.jsx)(App, {}, void 0), document.getElementById("root"));
function App() {
    return ((0, jsx_runtime_1.jsxs)(jsx_runtime_1.Fragment, { children: [(0, jsx_runtime_1.jsx)("h1", { children: "App list" }, void 0), (0, jsx_runtime_1.jsx)("p", { children: "Pong" }, void 0)] }, void 0));
}
//# sourceMappingURL=index.js.map