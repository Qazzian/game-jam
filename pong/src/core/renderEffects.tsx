import {useEffect} from "react";
import {PixelGameEngine, COLOURS, Entity} from "pixel-game-engine";
import {PongState} from "./PongReducer";

export function useRenderEffect(state: PongState, gameEngine: PixelGameEngine|null) {

	useEffect(() => {
		function renderOnUpdate(timestamp: number) {
			renderAll(state, gameEngine as PixelGameEngine, timestamp);
		}

		gameEngine?.on('update', renderOnUpdate);

		return () => {
			gameEngine?.off('update', renderOnUpdate);
		}
	}, [state, gameEngine]);
}

function renderAll(state: PongState, gameEngine: PixelGameEngine, timestamp:number) {
	// console.info('Render: ', state);
	gameEngine.fillBackground();
	renderPaddle(state.paddle1, gameEngine);
	renderPaddle(state.paddle2, gameEngine);
	renderBall(state.ball, gameEngine);
}

function renderPaddle(paddleState: Entity, gameEngine: PixelGameEngine) {
	const {x, y, width, height} = paddleState;
	gameEngine.fillRect(x, y, width, height, COLOURS.YELLOW);
}

function renderBall(ballState: Entity, gameEngine: PixelGameEngine) {
	const {x, y} = ballState;
	gameEngine.fillRect(x, y, 1, 1, COLOURS.CYAN);
}
