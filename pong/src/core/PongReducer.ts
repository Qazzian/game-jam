import {InputAction, InputActionTypes, keyboardReducer, keyEvents, KeyState} from "./KeyboardReducer";
import {defaultKeyBindings, InputState, keybindingReducer, KeyBindings, keyCodeToGameEvents} from "./KeyBindings";
import {collision, Entity, Position, Random} from 'pixel-game-engine';
import { TimeStats } from "pixel-game-engine/src/PixelGameEngine";


export interface PongState {
	width: number,
	height: number,
	isRunning: boolean,
	isMenuOpen: boolean,
	time: number,
	startTime: number,
	scores: number[],
	ball: Entity,
	paddle1: Entity,
	paddle2: Entity,
	rng: Random,
	rngSeed: string | undefined,
	inputs: KeyState,
	keyBindings: KeyBindings<InputState>,
}

export const initialState: PongState = {
	width: 80,
	height: 60,
	isRunning: false,
	isMenuOpen: true,
	time: 0,
	startTime: 0,
	scores: [
		0, 0,
	],
	ball: {
		x: 40.5,
		y: 30.5,
		width: 1,
		height: 1,
		vector: new Position(0.01, 0.01),
	} as Entity,
	paddle1: {
		x: 10,
		y: 0,
		width: 1,
		height: 60,
		vector: new Position(0, 0),
	} as Entity,
	paddle2: {
		x: 80 - 10,
		y: 0,
		width: 1,
		height: 60,
		vector: new Position(0, 0),
	} as Entity,
	rng: {} as Random,
	rngSeed: undefined,
	inputs: {} as KeyState,
	keyBindings: keybindingReducer(defaultKeyBindings),
}

enum Side {
	Top=1,
	Right,
	Bottom,
	Left,
}

interface Action {
	type: string,
	side?: Side,
	timeDiff?: number,
	scores?: number[],
}

const BALL_SPEED = new Position(0.01, 0.01);
const BALL_ACCELERATION = new Position(1.0002, 1.0002);


export const pongReducer = (state?: PongState, action?: Action| InputAction) : PongState => {
	if (!state) {
		return initialState;
	}
	if (!action){
		return state;
	}
	const newState = mainReducer(state, action);
	newState.ball = ballReducer(newState.ball, action);
	if (['keydown', 'keyup'].includes(action.type)) {
		newState.inputs = keyboardReducer(newState.inputs, action as InputAction);
	}

	return newState;
};

export const mainReducer = (state: PongState, action:Action): PongState => {
	switch (action.type) {
		case 'start':
			const rng = new Random();
			return {
				...state,
				isRunning: true,
				isMenuOpen: false,
				startTime: Date.now(),
				time: Date.now(),
				rng: rng,
				ball: {
					...state.ball,
					vector: generateBallVector(rng, BALL_SPEED),
				}
			};
		case 'pause':
			return {
				...state,
				isRunning: false,
				isMenuOpen: true,
			};
		case 'resume':
			return {
				...state,
				isRunning: true,
				isMenuOpen: false,
			}
		case 'reset':
			console.info('RESET');
			return initialState;
		case 'tick':
			if (!state.isRunning) {
				return state;
			}
			const oldTime = state.time;
			const newTime = Date.now();
			const timeDiff = newTime - oldTime;
			const keyEvents = keyCodeToGameEvents(state.inputs, state.keyBindings);
			// console.info('Tick time: ', {newTime, oldTime, timeDiff});
			return {
				...state,
				time: newTime,
				ball: ballReducer(state.ball, {
					...action,
					timeDiff,
				}),
				paddle1: movePaddle1(state.paddle1, keyEvents),
				paddle2: movePaddle2(state.paddle2, keyEvents),
			};
		case 'score':
			// TODO make a better score action (maybe it should have the player number instead)
			if (Array.isArray(action.scores)) {
				const newScores = action.scores.map((value, index) => state.scores[index] + value);
				return {
					...state,
					scores: newScores,
					ball: {
						...ballReducer(state.ball, {type: 'ballReset'}),
						vector: generateBallVector(state.rng, BALL_SPEED),
					},
				};
			}
			return state;
	}
	return state;
}

export const ballReducer = (ballState: Entity, action: Action): Entity => {
	if (!ballState) {
		return {
			...initialState.ball,
		};
	}
	if (!action) {
		return ballState;
	}
	switch (action.type) {
		case 'tick':
		case 'ballTick':
			const timeDiff = action.timeDiff || 0;
			return {
				...ballState,
				x: ballState.x + (ballState.vector.x * timeDiff),
				y: ballState.y + (ballState.vector.y * timeDiff),
				vector: ballState.vector.multiply(BALL_ACCELERATION),
			};
		case 'ballCollision':
			switch (action.side) {
				case Side.Top:
					return {
						...ballState,
						vector: new Position(
							ballState.vector.x,
							Math.abs(ballState.vector.y),
						),
					}
				case Side.Bottom:
					return {
						...ballState,
						vector: new Position(
							ballState.vector.x,
							Math.abs(ballState.vector.y) * -1,
						),
					}
				case Side.Left:
					return {
						...ballState,
						vector: new Position(
							Math.abs(ballState.vector.x),
							ballState.vector.y,
						),
					}
				case Side.Right:
					return {
						...ballState,
						vector: new Position(
							Math.abs(ballState.vector.x) * -1,
							ballState.vector.y,
						),
					}
				default:
					return ballState;
			}
		case 'ballReset':
			return initialState.ball;
	}

	return ballState;
}

function generateBallVector(rng: Random, speedMultiplyer: Position) {
	let ballVector: Position;
	do {
		ballVector = rng.vector().multiply(speedMultiplyer);
	}	while (Math.abs(ballVector.y) > Math.abs(ballVector.x));
	return ballVector;
}

function movePaddle1(paddleState: Entity, inputState: InputState) {
	const newState = {...paddleState};
	if (inputState.leftPaddleUp) {
		console.info('movePaddle1 up', );
		newState.y = newState.y - 1;
	}
	if (inputState.leftPaddleDown) {
		console.info('movePaddle1 down', );
		newState.y = newState.y + 1;
	}
	return newState;
}

function movePaddle2(paddleState: Entity, inputState: InputState) {
	const newState = {...paddleState};
	if (inputState.rightPaddleUp) {
		console.info('movePaddle2 up');
		newState.y = newState.y - 1;
	}
	if (inputState.rightPaddleDown) {
		console.info('movePaddle2 down');
		newState.y = newState.y + 1;
	}
	return newState;
}

export function getInputEffects(inputState: InputState, inputConfig: KeyBindings<InputState>): Action[] {
	return Object.keys(inputConfig)
		.filter((keyEventName ) => inputState[inputConfig[keyEventName]])
		.map((keyEventName) => ({
		type: keyEventName,
	}));
}

export function getCollisionActions(state:PongState, timePassed: number): Action[]{
	const {ball, width, height, paddle1, paddle2} = state;
	const actions = [];
	if (ball.x < 0) {
		actions.push(score(1, 1));
	}
	if (ball.x + ball.width >= width) {
		actions.push(score(0, 1));
	}
	if (ball.y <= 0) {
		console.info('COLLISION: top');
		actions.push(ballCollision(Side.Top))
	}
	if (ball.y + ball.height >= height) {
		console.info('COLLISION: bottom');
		actions.push(ballCollision(Side.Bottom))
	}
	if (collision(ball, paddle1, timePassed)) {
		console.info('COLLISION: Paddle left');
		actions.push(ballCollision(Side.Left))
	}
	if (collision(ball, paddle2, timePassed)) {
		console.info('COLLISION: Paddle right');
		actions.push(ballCollision(Side.Right))
	}
 
	return actions;
}




export function startGame(): Action {
	return {
		type: 'start',
	};
}

export function pauseGame(): Action {
	return {
		type: 'pause',
	};
}

export function resumeGame(): Action {
	return {
		type: 'resume',
	};
}

export function resetGame(): Action {
	return {
		type: 'reset',
	};
}

export function tick(timeStats: TimeStats): Action {
	return {
		type: 'tick',
		...timeStats
	}
}

export function score(playerIndex:number, scoreDiff=1) : Action {
	const scores = [...initialState.scores];
	scores[playerIndex] = scoreDiff;
	return {
		type: 'score',
		scores,
	}
}

export function ballCollision(side: Side): Action {
	return {
		type: 'ballCollision',
		side,
	};
}
