import { KeyState } from "./KeyboardReducer";


export type KeyBindings<Type> = {
	[Property: string]: keyof Type;
}

export interface ConfigAction<Type> {
	type: 'updateConfig',
	payload: KeyBindings<Type>,
}


export interface InputState {
	leftPaddleUp?: boolean,
	leftPaddleDown?: boolean,
	rightPaddleUp?: boolean,
	rightPaddleDown?: boolean,
	menu?: boolean,
	pause?: boolean,
	start?: boolean,
}

export const defaultKeyBindings: KeyBindings<InputState>  = {
	w: 'leftPaddleUp',
	s: 'leftPaddleDown',
	ArrowUp: 'rightPaddleUp',
	ArrowDown: 'rightPaddleDown',
	esc: 'menu',
	p: 'pause',
	space: 'start',
};

export type InputKeyNames = keyof InputState;


export function keybindingReducer<Type> (state: KeyBindings<Type>, configAction?: ConfigAction<Type>): KeyBindings<Type> {
	if (!configAction) { return state; }

	if (configAction.type === 'updateConfig') {
		return {
			...state,
			...configAction.payload,
		}
	}

	return state;
}

export function keyCodeToGameEvents (inputState: KeyState, keyBindings: KeyBindings<InputState>): InputState {
	const out: InputState = {}

	for (let keyCode in inputState) {
		if (inputState[keyCode]) {
			const actionName = keyBindings[keyCode];
			out[actionName] = true;
		}
	}
	return out;
}