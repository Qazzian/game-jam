import {KeyBindings} from "./KeyBindings";


export interface KeyState {
	[key: string]: boolean,
}

export type InputActionTypes =  'keydown' | 'keyup';

export interface InputAction {
	type: InputActionTypes,
	keyCode: string,
}


export function keyboardReducer(state: KeyState, action: InputAction): KeyState {
	if (!state) { return {} as KeyState; }

	if (!action) { return state; }

	if (action.type === 'keydown') {
		return {
			...state,
			[action.keyCode]: true,
		}
	}
	if (action.type === 'keyup') {
		return {
			...state,
			[action.keyCode]: false,
		}
	}

	return state;
}


export function keyEvents(keyCode: string, isDown: boolean): InputAction {
	return {
		type: isDown ? 'keydown' : 'keyup',
		keyCode,
	} as InputAction;
}
