import React, {ReactNode, useContext, useReducer} from "react";
import {PongState, pongReducer, initialState} from './PongReducer';

interface PongContextInterface {
	state: PongState,
	dispatch: React.Dispatch<any>,
}

// TODO use PongReducer instead of multiple useState's
const context = React.createContext<PongContextInterface>({
	state: initialState,
	dispatch: () => null,
});

export const usePongContext = () => useContext(context);


type ProviderProps = {
	children: ReactNode,
};

export const PongContextProvider = ({children}: ProviderProps) => {
	const [state, dispatch] = useReducer(pongReducer, initialState, pongReducer);

	return (
		<context.Provider value={{state, dispatch}}>
			{children}
		</context.Provider>
	)
}
