import {keyboardReducer, KeyState, keyEvents} from "./KeyboardReducer";

describe('InputReducer', () => {
	test('defined',  () => {
		expect(keyboardReducer).toBeDefined();
		expect(typeof keyboardReducer).toBe('function');
		expect(keyEvents).toBeDefined();
		expect(typeof keyEvents).toBe('function');
	});

	test('create Key Actions',  () => {
		const sDownAction = keyEvents('S', true);
		expect(sDownAction).toBeDefined();
		expect(sDownAction.type).toBe('keydown');
		const sUpAction = keyEvents('S', false);
		expect(sUpAction).toBeDefined();
		expect(sUpAction.type).toBe('keyup');
	});

	test('Key actions saved in the state',  () => {
		const sDownEvent = new KeyboardEvent('keydown', {'key': 'S'});
		const sDownAction = keyEvents('S', true);
		const downState = keyboardReducer({} as KeyState, sDownAction);
		expect(downState).toMatchObject({'S': true});
	});
});
