import {Position, Random} from 'pixel-game-engine';

import { defaultKeyBindings, keyCodeToGameEvents } from './KeyBindings';
import {getInputEffects, initialState, pauseGame, pongReducer, PongState, score, startGame, tick} from './PongReducer';

describe('PongReducer', () => {
	test('initialState',  () => {
		const testInit = pongReducer();
		expect(testInit).toBeDefined();
		expect(testInit).toMatchObject(initialState);
		const unalteredState = pongReducer(testInit);
		expect(unalteredState).toMatchObject(initialState);
	});
	
	test('Change Running States',  () => {
		const initState = pongReducer();
		expect(initState.isRunning).toBe(false);
		expect(initState.isMenuOpen).toBe(true);
		const startedState = pongReducer(initState, startGame());
		expect(startedState.isRunning).toBe(true);
		expect(startedState.isMenuOpen).toBe(false);
		const pausedState = pongReducer(startedState, pauseGame());
		expect(pausedState.isRunning).toBe(false);
		expect(pausedState.isMenuOpen).toBe(true);

	});

	test('Score system',  () => {
		const testState = {
			...initialState,
			rng: new Random('1'),
		};

		const state1 = pongReducer(testState, score(0, 1));
		expect(state1.scores).toMatchObject([1, 0]);
		const state2 = pongReducer(testState, score(1, -1));
		expect(state2.scores).toMatchObject([0, -1]);
	});

	test('Ball movement',  () => {
		const startState: PongState = {
			...initialState,
			ball: {
				...initialState.ball,
				x: 10,
				y: 0,
				vector: new Position(1, 1),
			}
		}
		const state1 = pongReducer(startState, tick({
			timestamp: Date.now(),
			timePassed: 1,
			fps: 60,
		}));
		expect(state1.ball.vector).toMatchObject({x: 1, y: 1});
	});
});
