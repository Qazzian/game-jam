import React from 'react';
import './App.css';
import PixelCanvas from "./componants/PixelCanvas";
import {PongContextProvider} from "./core/PongContext";
import MenuScreen from "./componants/MenuScreen";
import {Score} from "./componants/Score";

function App() {
	console.info('RENDER APP');
	
	return (
		<div className="App">
			<header className="App-header">Pong</header>
			<PongContextProvider>
				<Score/>
				<MenuScreen/>
				<PixelCanvas/>
			</PongContextProvider>


		</div>
	);
}

export default App;
