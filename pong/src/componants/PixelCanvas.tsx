import React, {useEffect, useRef} from "react";

import {PixelGameEngine} from 'pixel-game-engine';
import { usePongContext} from "../core/PongContext";
import {tick, getCollisionActions} from "../core/PongReducer";
import {keyEvents} from "../core/KeyboardReducer";
import {useRenderEffect} from "../core/renderEffects";
import { TimeStats } from "pixel-game-engine/src/PixelGameEngine";

export default function PixelCanvas() {
	const canvasRef = useRef(null);
	const engineRef = useRef<PixelGameEngine|null>(null);
	const {state, dispatch} = usePongContext();

	// create engine instance and attache canvas element
	useEffect(() => {
		const canvasNode = canvasRef.current;
		if (canvasNode === null) {
			return;
		}

		engineRef.current = new PixelGameEngine(canvasNode, 800, 600, state.width, state.height);
	}, [state.width, state.height]);

	// Is running state
	useEffect(() => {
		const gameEngine = engineRef.current;

		if (state.isRunning) {
			gameEngine?.start();
		}
		else {
			gameEngine?.stop();
		}

		return () => {
			gameEngine?.stop();
		};
	}, [state.isRunning]);

	// On frame tick dispatch automatic actions
	useEffect(() => {
		const engine = engineRef.current;
		function updateState(timeStats: TimeStats) {
			// console.info('on Game update: ', timestamp);
			const {timePassed} = timeStats;
			getCollisionActions(state, timePassed).forEach((action) => {
				dispatch(action);
			});
			dispatch(tick(timeStats));
		}
		engine?.on('update', updateState);

		return () => {
			engine?.off('update', updateState);
		}

	}, [state, dispatch]);

	// user input effects
	useEffect(() => {
		const windowAbort = new AbortController();

		window.addEventListener('keydown', (keyDownEvent: KeyboardEvent) => {
			const keyCode = keyDownEvent.key;
			dispatch(keyEvents(keyCode, true));
		}, {signal: windowAbort.signal});
		window.addEventListener('keyup', (keyUpEvent: KeyboardEvent) => {
			const keyCode = keyUpEvent.key;
			dispatch(keyEvents(keyCode, false));
		}, {signal: windowAbort.signal})

		return () => {
			windowAbort.abort();
		}
	}, [dispatch]);

	// on frame tick Render state changes
	useRenderEffect(state, engineRef.current);


	return (
		<div className="PixelCanvas">
			<canvas ref={canvasRef}/>
		</div>
	);
};
