import {usePongContext} from "../core/PongContext";
import './Score.scss';
import {useMemo} from "react";

export function Score() {
	const scores = usePongContext().state.scores;

	return useMemo(() => (
		<div className="Score">
			{scores.map((score, index) => (
				<span key={`p${index}`} id={`p${index}`}>{score}</span>
			))}
		</div>
	), [scores]);
}
