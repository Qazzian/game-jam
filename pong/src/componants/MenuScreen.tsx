import React from 'react';
import {usePongContext} from "../core/PongContext";

import "./MenuScreen.scss";
import {resumeGame, startGame} from "../core/PongReducer";

export default function MenuScreen() {
	const pongContext = usePongContext();

	if (!pongContext.state.isMenuOpen) {
		return <></>;
	}

	return (
		<div className="PongMenuScreen">
			<nav>
				{pongContext.state.time > 0 &&<button onClick={handleResume}>Resume Game</button> }
				<button onClick={handleNewGame}>New Game</button>
			</nav>
		</div>
	)

	function handleNewGame() {
		pongContext.dispatch(startGame());
	}

	function handleResume() {
		pongContext.dispatch(resumeGame());
	}


}
